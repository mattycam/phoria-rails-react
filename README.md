# README

This is a simple Rails (5.2) application with a react front-end for Phoria's coding challenge.

## Dependencies
- Ruby 2.5.1
- Bundler
- Rails 5.2

## Running the app
To install the app, simply extract the application to your desired location and run the following commands:

Run `bundle install` to install the required gems and their dependencies

Once finished installing, run `rails db:setup`.

This should set up the database required (sql-lite), create the appropriate tables, and run the seed file to create the initial products.

Once the appllication has been set up, run `rails server` to start the development server.

Navigate to [localhost:3000](http://localhost:3000) to view the application.


## Testing
Run `rails test` and `rails test:system` to run the full suite of unit, controller and system tests.


## Assumptions / Notes

### Managing Products and Orders
Both products and orders can be managed in the admin section of the application. I've added links in the navbar for convenience. In a real application this would authenticate users before allowing access, but I thought adding that would be a step too far for this demo.

Products can, however, be added, edited, removed, disabled etc, and will be able to be added to the cart as per the seeded products.

The admin part of the site is just vanilla rails (not react), and I would add an admin layout and better displaying/management of orders if this were a real site. Obviously with a real store there would be customer information too.

### Misc
I built the store with a few extra tidbits for functionality (e.g. taxes, in_stock_qty, statuses for both orders and products) with the view that any functional store would need to keep track of these. These features have not been fully built into the application, but it would be easy to extend the functionality of the app further to include these.

I've included the master.key file in this git repository. Normally this should not be included.

This repo can be found at [https://bitbucket.org/mattycam/phoria-rails-react/](https://bitbucket.org/mattycam/phoria-rails-react/)