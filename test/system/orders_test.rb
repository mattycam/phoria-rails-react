require "application_system_test_case"

class OrdersTest < ApplicationSystemTestCase
  setup do
    @high_quality = products(:high_quality)
    @premium = products(:premium)
  end

  test "visiting the shop" do
    visit root_url
    assert_selector "h2", text: "OUR PRODUCTS"
  end

  test "adding an item to the cart" do
    add_product_to_cart(@high_quality)
    visit cart_url
    assert_text @high_quality.name
    assert_text 2
  end

  def add_product_to_cart(product)
    visit root_url
    fill_in "qty-#{product.id}", with: 2
    within("#product-#{product.id}") { click_button('Add to Cart') }

    # to counteract delay on ajax request
    sleep(1)
  end

  test "finalising order" do
    add_product_to_cart(@high_quality)
    visit cart_url
    click_button "Order Now"

    assert_text "Success!"
  end
end
