# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  discount       :decimal(10, 2)   default(0.0)
#  status         :integer          default("pending")
#  subtotal       :decimal(10, 2)   default(0.0)
#  taxes          :decimal(10, 2)   default(0.0)
#  total          :decimal(10, 2)   default(0.0)
#  total_shipping :decimal(10, 2)   default(0.0)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  setup do
    @order = Order.new
  end

  test "should update subtotal before save" do
    assert_changes '@order.subtotal' do
      order_item = @order.order_items.build(product: products(:high_quality), quantity: 2)
      order_item.save
    end
  end

  test "should calculate subtotal before save" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 2)
    order_item.save

    assert_equal @order.subtotal, products(:high_quality).price * 2
  end

  test "should update total before save" do
    assert_changes '@order.total' do
      order_item = @order.order_items.build(product: products(:high_quality), quantity: 2)
      order_item.save
    end
  end

  test "should calculate total before save" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 5)
    order_item.save

    assert_equal @order.total, 130
  end

  test "should calculate total with free shipping" do
    order_item = @order.order_items.build(product: products(:premium), quantity: 12)
    order_item.save

    assert_equal @order.total, 360
  end

  test "should calculate shipping before save when needed" do
    order_item = @order.order_items.build(product: products(:premium), quantity: 3)
    order_item.save

    assert_equal @order.total_shipping, 30
  end

  test "should automatically apply free shipping" do
    order_item = @order.order_items.build(product: products(:premium), quantity: 12)
    order_item.save

    assert_equal @order.total_shipping, 0
  end

  test "should calculate discount" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 25)
    order_item.save

    assert_equal @order.discount, 50
  end

  test "should apply discount to total" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 25)
    order_item.save

    assert_equal @order.total, 450
  end

  test "should calculate total with discount" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 20)
    order_item.save
    order_item2 = @order.order_items.build(product: products(:premium), quantity: 10)
    order_item2.save

    assert_equal @order.total, 630
  end

  test "should return total quantity" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 20)
    order_item.save
    order_item2 = @order.order_items.build(product: products(:premium), quantity: 10)
    order_item2.save

    assert_equal @order.total_quantity, 30
  end
end
