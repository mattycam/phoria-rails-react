# == Schema Information
#
# Table name: order_items
#
#  id                     :integer          not null, primary key
#  quantity               :integer          default(1)
#  total_order_item_price :decimal(10, 2)   default(0.0)
#  unit_price             :decimal(10, 2)   default(0.0)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  order_id               :integer
#  product_id             :integer
#

require 'test_helper'

class OrderItemTest < ActiveSupport::TestCase
  setup do
    @order = Order.new
  end

  test "should save unit_price before save" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 2)
    order_item.save

    assert_equal order_item.unit_price, products(:high_quality).price
  end

  test "should save total_order_item_price before save" do
    order_item = @order.order_items.build(product: products(:high_quality), quantity: 2)
    order_item.save

    assert_equal order_item.total_order_item_price, 40
  end

  test "should update order before save" do
    assert_changes '@order.updated_at' do
      order_item = @order.order_items.build(product: products(:high_quality), quantity: 2)
      order_item.save
    end
  end
end
