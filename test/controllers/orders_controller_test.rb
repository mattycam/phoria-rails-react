require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    get get_current_order_url, xhr: true
    current_order_id = JSON.parse(@response.body)['id']
    @current_order = Order.find(current_order_id)
  end

  test "should get current_order" do
    get get_current_order_url, xhr: true
    assert_equal "application/json", @response.content_type
  end

  test "should get current_order_with_items" do
    get get_current_order_with_items_url, xhr: true
    assert_equal "application/json", @response.content_type
  end

  test "should add new item" do
    post add_item_to_current_order_url, params: {
      order_item: {
        product_id: products(:high_quality).id,
        quantity: 1
      }
    }
    assert_equal @current_order.order_items.size, 1
  end

  test "should update existing item" do
    2.times do
      post add_item_to_current_order_url, params: {
        order_item: {
          product_id: products(:high_quality).id,
          quantity: 2
        }
      }
    end

    assert_equal 1, @current_order.order_items.size
    assert_equal @current_order.total_quantity, 4
  end

  test "should remove item" do
    post add_item_to_current_order_url, params: {
      order_item: {
        product_id: products(:high_quality).id,
        quantity: 1
      }
    }

    post add_item_to_current_order_url, params: {
      order_item: {
        product_id: products(:premium).id,
        quantity: 1
      }
    }
    assert_equal @current_order.order_items.size, 2
    first_item = @current_order.order_items.first

    post remove_item_from_cart_url, params: { item_id: first_item.id }
    refute_includes @current_order.order_items, first_item
  end

  test "should show cart" do
    get cart_url
    assert_response :success
  end

  test "should finalise order and redirect" do
    post finalise_order_url
    updated_order = Order.find(@current_order.id)
    assert updated_order.complete?
    assert_redirected_to thankyou_url
  end
end
