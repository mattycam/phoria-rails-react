json.extract! product, :id, :name, :description, :price, :sku, :in_stock_qty, :status, :created_at, :updated_at
json.url product_url(product, format: :json)
