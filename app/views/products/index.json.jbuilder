json.array! @products do |product|
  json.extract! product, :id, :name, :description, :price
  if product.product_image.attached?
    json.product_image_url rails_blob_path(product.product_image, disposition: "attachment")
  end
end