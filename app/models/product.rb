# == Schema Information
#
# Table name: products
#
#  id           :integer          not null, primary key
#  description  :text
#  in_stock_qty :integer
#  name         :string
#  price        :decimal(10, 2)
#  sku          :string
#  status       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Product < ApplicationRecord
  has_one_attached :product_image

  enum status: %i[disabled active]
end
