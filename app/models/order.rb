# == Schema Information
#
# Table name: orders
#
#  id             :integer          not null, primary key
#  discount       :decimal(10, 2)   default(0.0)
#  status         :integer          default("pending")
#  subtotal       :decimal(10, 2)   default(0.0)
#  taxes          :decimal(10, 2)   default(0.0)
#  total          :decimal(10, 2)   default(0.0)
#  total_shipping :decimal(10, 2)   default(0.0)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Order < ApplicationRecord
  has_many :order_items, dependent: :destroy

  enum status: %i[pending complete cancelled refunded]

  before_save :update_subtotal, :update_shipping, :update_discount, :update_total

  def total_quantity
    order_items.pluck(:quantity).sum
  end

  def to_builder
    items = order_items.includes(:product)

    Jbuilder.new do |order|
      order.(self, :id, :subtotal, :total, :total_shipping, :taxes, :discount)
      order.order_items items do |item|
        order.id item.id
        order.product_name item.product.try(:name)
        order.quantity item.quantity
        order.unit_price item.unit_price
        order.total_price item.total_order_item_price
      end
    end
  end

  private

  def update_subtotal
    self.subtotal = order_items.pluck(:total_order_item_price).sum
  end

  def update_shipping
    qty = total_quantity
    standard_shipping_cost = 30
    free_shipping_qty = 10

    if qty > 0 && qty < free_shipping_qty
      self.total_shipping = standard_shipping_cost
    else
      self.total_shipping = 0
    end
  end

  def update_discount
    discount_rate = 0.1
    min_discount_qty = 20
    self.discount = total_quantity > min_discount_qty ? subtotal * discount_rate : 0
  end

  def update_total
    self.total = (subtotal + taxes + total_shipping) - discount
  end
end
