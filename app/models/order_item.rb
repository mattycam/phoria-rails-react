# == Schema Information
#
# Table name: order_items
#
#  id                     :integer          not null, primary key
#  quantity               :integer          default(1)
#  total_order_item_price :decimal(10, 2)   default(0.0)
#  unit_price             :decimal(10, 2)   default(0.0)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  order_id               :integer
#  product_id             :integer
#

class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  before_save :update_unit_price, :update_total_order_item_price
  after_save :update_order
  after_destroy :update_order

  def update_unit_price
    self.unit_price = Product.find(product_id).price
  end

  def update_total_order_item_price
    self.total_order_item_price = quantity * unit_price
  end

  def update_order
    order.save
  end
end
