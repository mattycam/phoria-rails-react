class OrdersController < ApplicationController

  def get_current_order
    render json: current_order
  end

  def get_current_order_with_items
    render json: current_order.to_builder.attributes!
  end

  def add_item_to_current_order
    # check if item already exists in order
    existing_item = current_order.order_items.find_by(product_id: params[:order_item][:product_id])

    existing_item.present? ? update_existing_item(existing_item) : add_new_item
  end

  def remove_item_from_current_order
    item = current_order.order_items.find(params[:item_id])
    item.destroy!
    render json: current_order.to_builder.attributes!
  end

  def cart
  end

  def finalise
    current_order.complete!
    redirect_to thankyou_path
  end

  def thankyou
    # save order for displaying any custom thankyou message, and reset
    @completed_order = current_order
    create_new_order
  end

  private

  def order_item_params
    params.require(:order_item).permit(
      :product_id,
      :quantity
    )
  end

  def add_new_item
    @new_order_item = current_order.order_items.build(order_item_params)
    if @new_order_item.save
      render json: current_order
    else
      render :json => { errors: @updated_order.errors.full_messages},
        status: 422
    end
  end

  def update_existing_item(order_item)
    order_item.quantity = order_item.quantity + params[:order_item][:quantity].to_i
    if order_item.save
      render json: current_order
    else
      render :json => { errors: order_item.errors.full_messages}
    end
  end
end
