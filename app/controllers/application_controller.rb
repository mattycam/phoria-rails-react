class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  helper_method :current_order, :create_new_order

  def current_order
    if session[:current_order_id].present?
      Order.find_by(id: session[:current_order_id]) || create_new_order
    else
      create_new_order
    end
  end

  def create_new_order
    new_order = Order.create
    session[:current_order_id] = new_order.id
    new_order
  end
end
