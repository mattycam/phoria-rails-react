class CartSummary extends React.Component {
  render() {
    var subtotal = accounting.formatMoney(this.props.total);
    return <li className="navbar-text font-weight-bold cart-summary">Total: {subtotal}</li>
  }
}