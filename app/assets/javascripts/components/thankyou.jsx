class Thankyou extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentOrder: {}
    }
    this.getCurrentOrder = this.getCurrentOrder.bind(this);
  }

  componentDidMount() {
    this.getCurrentOrder();
  }

  getCurrentOrder() {
    $.ajax({
      type: 'GET',
      url: '/get_current_order.json',
      success: currentOrder => {
        this.setState({ currentOrder });
      },
      error: response => {
        console.log('Error getting currentOrder: ', response);
      }
    });
  }

  render() {
    return (
      <div className="thankyou-page">
        <Header currentOrder={this.state.currentOrder}/>

        <div className="container my-5">
          <h1>Success!</h1>

          <h3 className="my-4">Your order number is <span className="font-weight-bold">{this.props.order.id}</span></h3>
          <p className="lead-text">Soon you will be the owner of some great new VR Cardboard products!</p>
          <p>Thanks for shopping at VR Cardboard Store.</p>
        </div>
      </div>
    )
  }
}