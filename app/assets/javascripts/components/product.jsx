class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: 1
    }
    this.addItemToCart = this.addItemToCart.bind(this);
  }

  addItemToCart(productId) {
    // animation
    var $productImage = $(`#product-image-${this.props.product.id}`);
    flyToCart($productImage);

    // wait for animation to finish
    setTimeout(
      () => this.props.addItemToCart(productId, this.state.qty), 500
    );
  }
  render() {
    const {
      product,
      addItemToCart
    } = this.props;

    return (
      <div className="product my-5" id={`product-${product.id}`}>
        <div className="row">
          <div className="col-sm">
            <img src={product.product_image_url || 'http://via.placeholder.com/540x298'} className="img-fluid" id={`product-image-${product.id}`} />
          </div>

          <div className="col-sm">
            <div className="row">
              <div className="col-12 mb-3">
                <h3>{product.name}</h3>
              </div>
            </div>

            <div className="row form-inline mb-2">
              <div className="col-2 col-xs col-sm-4">
                <p className="price">{accounting.formatMoney(product.price)}</p>
              </div>
              <div className="col-md-12 col-lg">
                <div className="input-group mb-2 inline">
                  <div className="input-group-prepend">
                    <div className="input-group-text">Qty</div>
                  </div>
                  <input
                    value={this.state.qty}
                    type="number"
                    min="1"
                    onChange={e => this.setState({ qty: e.target.value })}
                    className="form-control mr-3"
                    name={`qty-${product.id}`}
                  />
                  <button onClick={() => this.addItemToCart(product.id)} className="btn btn-primary">Add to Cart</button>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col">
                <p>{product.description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}