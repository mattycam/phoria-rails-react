class Header extends React.Component {
  render() {
    return (
      <div className="nav-wrapper">

        <nav className="navbar navbar-expand-sm navbar-light bg-light">
          <div className="container-fluid">
            <a href="/" className='navbar-brand'>VR Cardboard Shop</a>
            <div id="navbar">
              <ul className="navbar-nav ml-auto">
                <li><a href="/admin/products" className="scroll-to nav-link mr-3">Manage Products</a></li>
                <li><a href="/admin/orders" className="scroll-to nav-link mr-3">Manage Orders</a></li>
                <li><a href="/cart" className="scroll-to mr-3 btn btn-success">View Cart</a></li>
                <CartSummary total={this.props.currentOrder.subtotal} />
              </ul>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}