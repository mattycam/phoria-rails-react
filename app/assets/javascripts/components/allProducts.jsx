class AllProducts extends React.Component {
  render() {
    const {
      products,
      addItemToCart
    } = this.props;

    var allProducts = products.map((product) => {
      return <Product product={product} key={product.id} addItemToCart={addItemToCart} />
    });

    return (
      <div className="all-products mt-4">
        <h2>OUR PRODUCTS</h2>
        <div className="divider"></div>
        {allProducts}
      </div>
    )
  }
}