class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentOrder: {
        order_items: [],
        subtotal: 0,
        total_shipping: 0,
        taxes: 0,
        total: 0
      }
    }
    this.getCurrentOrderWithItems = this.getCurrentOrderWithItems.bind(this);
    this.finaliseOrder = this.finaliseOrder.bind(this);
    this.removeItemFromCart = this.removeItemFromCart.bind(this);
  }

  componentDidMount() {
    this.getCurrentOrderWithItems();
  }

  getCurrentOrderWithItems() {
    $.ajax({
      type: 'GET',
      url: '/get_current_order_with_items.json',
      success: currentOrder => {
        this.setState({ currentOrder });
      },
      error: response => {
        console.log('Error getting current order with items: ', response);
      }
    });
  }

  finaliseOrder() {
    $.ajax({
      type: 'POST',
      url: '/finalise_order',
      success: response => {
        console.log(response);
      },
      error: response => {
        console.log('Error finalising order: ', response);
      }
    });
  }

  removeItemFromCart(itemId) {
    $.ajax({
      type: 'POST',
      url: '/remove_item_from_cart',
      data: {
        item_id: itemId
      },
      success: currentOrder => {
        this.setState({ currentOrder });
      },
      error: response => {
        console.log('Error removing item from cart: ', response);
      }
    });
  }


  render() {
    const items = this.state.currentOrder.order_items;
    var currentOrder = this.state.currentOrder;
    var orderItems = items.map((item) => {
      return (
        <tr key={item.id}>
          <td>
            <button
              className="btn btn-danger btn-sm"
              onClick={() => this.removeItemFromCart(item.id)}>
              x
            </button>
          </td>
          <td>{item.product_name}</td>
          <td>{item.quantity}</td>
          <td>{accounting.formatMoney(item.unit_price)}</td>
          <td className="text-right">{accounting.formatMoney(item.total_price)}</td>
        </tr>
        )
    });

    return (
      <div>
        <Header currentOrder={currentOrder}/>

        <div className="container my-5">
          <h1 className="mb-5">Shopping Cart</h1>
          <table className="table">
            <thead>
              <tr>
                <th></th>
                <th>Item</th>
                <th>Qty</th>
                <th>Unit Price</th>
                <th className="text-right">Total</th>
              </tr>
            </thead>

            <tbody>
              {orderItems}
              <tr>
                <td colSpan="3"></td>
                <td className="font-weight-bold">Subtotal</td>
                <td className="text-right">{accounting.formatMoney(currentOrder.subtotal)}</td>
              </tr>
              <tr>
                <td colSpan="3"></td>
                <td className="font-weight-bold">Shipping Fees</td>
                <td className="text-right">{accounting.formatMoney(currentOrder.total_shipping)}</td>
              </tr>
              <tr>
                <td colSpan="3"></td>
                <td className="font-weight-bold">Taxes</td>
                <td className="text-right">{accounting.formatMoney(currentOrder.taxes)}</td>
              </tr>
              { currentOrder.discount > 0 &&
                <tr>
                  <td colSpan="3"></td>
                  <td className="font-weight-bold">Bulk Discount</td>
                  <td className="font-weight-bold text-success text-right">-{accounting.formatMoney(currentOrder.discount)}</td>
                </tr>
              }
              <tr>
                <td colSpan="3"></td>
                <td className="font-weight-bold">Total</td>
                <td className="font-weight-bold text-right">{accounting.formatMoney(currentOrder.total)}</td>
              </tr>
            </tbody>
          </table>

          <div className="text-right">
            <button className="btn btn-success btn-lg" onClick={() => this.finaliseOrder()}>Order Now</button>
          </div>
        </div>
      </div>
    )
  }
}