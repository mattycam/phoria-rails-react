class Shop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      currentOrder: {}
    }
    this.addItemToCart = this.addItemToCart.bind(this);
    this.getCurrentOrder = this.getCurrentOrder.bind(this);
  }

  componentDidMount() {
    this.getAllProducts();
    this.getCurrentOrder();
  }

  getAllProducts() {
    $.ajax({
      type: 'GET',
      url: '/products.json',
      success: products => {
        this.setState({ products });
      },
      error: response => {
        console.log('Error getting products: ', response);
      }
    });
  }

  getCurrentOrder() {
    $.ajax({
      type: 'GET',
      url: '/get_current_order.json',
      success: currentOrder => {
        this.setState({ currentOrder });
      },
      error: response => {
        console.log('Error getting currentOrder: ', response);
      }
    });
  }

  addItemToCart (productId, quantity) {
    $.ajax({
      type: 'POST',
      url: '/add_item_to_current_order.json',
      data: {
        order_item: {
          product_id: productId,
          quantity
        }
      },
      success: currentOrder => {
        this.getCurrentOrder();
      },
      error: response => {
        console.log('Error adding item to cart: ', response);
      }
    });
  }

  render() {
    return (
      <div className="shop">
        <Header currentOrder={this.state.currentOrder}/>
        <Banner />

        <div className="container">
          <AllProducts products={this.state.products} addItemToCart={this.addItemToCart} />
        </div>
      </div>
    )
  }
}