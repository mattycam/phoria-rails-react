function flyToCart($productImage) {
  var cart = $('.cart-summary');
  if ($productImage) {
    var imgclone = $productImage.clone()
      .offset({
        top: $productImage.offset().top,
        left: $productImage.offset().left
     })
      .css({
      'opacity': '0.5',
      'position': 'absolute',
      'height': '150px',
      'width': '150px',
      'z-index': '100'
    })
      .appendTo($('body'))
      .animate({
        'top': cart.offset().top + 10,
        'left': cart.offset().left + 10,
        'width': 75,
        'height': 75
    }, 500);

    imgclone.animate({
        'width': 0,
            'height': 0
    }, function () {
        $(this).detach()
    });
  }
}