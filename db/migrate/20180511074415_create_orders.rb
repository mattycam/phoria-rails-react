class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.decimal :total, precision: 10, scale: 2, default: 0
      t.decimal :subtotal, precision: 10, scale: 2, default: 0
      t.decimal :taxes, precision: 10, scale: 2, default: 0
      t.decimal :total_shipping, precision: 10, scale: 2, default: 0
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
