class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :product_id
      t.integer :quantity, default: 1
      t.decimal :unit_price, precision: 10, scale: 2, default: 0
      t.decimal :total_order_item_price, precision: 10, scale: 2, default: 0

      t.timestamps
    end
  end
end
