class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.decimal :price, precision: 10, scale: 2
      t.string :sku
      t.integer :in_stock_qty
      t.integer :status

      t.timestamps
    end
  end
end
