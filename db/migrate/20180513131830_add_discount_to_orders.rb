class AddDiscountToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :discount, :decimal, precision: 10, scale: 2, default: 0
  end
end
