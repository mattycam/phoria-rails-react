# Create Products
high_quality = Product.new(
  name: 'VR Cardboard High Quality',
  description: 'High quality, durable VR Cardboard. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, assumenda omnis dolorem, tempora reprehenderit voluptate veritatis hic eaque, reiciendis, excepturi neque doloribus. Dicta ad distinctio, rerum porro voluptatum? Nulla, aperiam! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo assumenda!',
  price: 20,
  sku: 'VR100',
  in_stock_qty: 1000,
  status: :active
)
high_quality.product_image.attach(io: File.open("#{Rails.root}/app/assets/images/goggles.jpg"), filename: "goggles.jpg", content_type: "image/jpg")
high_quality.save

premium = Product.new(
  name: 'VR Cardboard Premium',
  description: 'The best VR Cardboard available on the market! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, assumenda omnis dolorem, tempora reprehenderit voluptate veritatis hic eaque, reiciendis, excepturi neque doloribus. Dicta ad distinctio, rerum porro voluptatum? Nulla, aperiam!',
  price: 30,
  sku: 'VR150',
  in_stock_qty: 1000,
  status: :active
)
premium.product_image.attach(io: File.open("#{Rails.root}/app/assets/images/goggles-premium.jpg"), filename: "goggles-premium.jpg", content_type: "image/jpg")
premium.save
