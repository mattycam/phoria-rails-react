Rails.application.routes.draw do
  root 'home#index'

  resources :products, only: :index

  namespace :admin do
    resources :products
    resources :orders
  end

  post :add_item_to_current_order, to: 'orders#add_item_to_current_order'
  get :get_current_order, to: 'orders#get_current_order'
  get :get_current_order_with_items, to: 'orders#get_current_order_with_items'
  get :cart, to: 'orders#cart'
  post :remove_item_from_cart, to: 'orders#remove_item_from_current_order'
  post :finalise_order, to: 'orders#finalise'
  get :thankyou, to: 'orders#thankyou'
end
